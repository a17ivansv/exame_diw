/******************************************************
 * Crea unha función que cando usemos a grid-gallery
 * amose as imaxes cuadradas.
 * Precisarás chamala co evento 'resize' para
 * recalcular o alto cando cambie o tamaño de pantalla
 ******************************************************/

// Función que devolve o ancho do elemento
getElementWidth = (element) => { return element.getBoundingClientRect().width; }

// Función para facer scroll ata un elemento html
scrollToElement = (element) => { element.scrollIntoView({ behavior: 'smooth', block: 'start' });}

var x = document.getElementsByClassName("menu-item");
for(let i = 0;i<x.length;i++)
{
    let nuevaurl="";
    
    x[i].addEventListener(
        'click',function(){
            nuevaurl = "#"+x[i].getAttribute('data-target');
            location.href = "./"+nuevaurl;
        },false
    );
}
//Llamamos a redimensionar para que se redimensionen en la primera ejecucion aunque no redimensionemos la pagina
redimensionar();


window.addEventListener(
    'resize',redimensionar,false
);

function redimensionar()
{
    let li = document.getElementsByClassName('gallery-item');
    for(let i=0;i<li.length;i++)
    {
        let actual_item = li[i];
       actual_item.style.height = getElementWidth(actual_item)+"px";
    }
}